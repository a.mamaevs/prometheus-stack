# Prometheus monitoring stack

   Prometheus,
   Grafana,
   Node Exporter,
   Blackbox Exporter,
   AlertManager.
   
Скопировать файл .grafanaenv.example в .grafanaenv
```
cp .grafanaenv.example .grafanaenv
```
Задать логин и пароль grafana в файле.
 Отредактироваd файл .grafanaenv
```
nano .grafanaenv
```

Скопировать файл ./config/blackbox-targets.json.example в ./config/blackbox-targets.json
```
cp ./config/blackbox-targets.json.example ./config/blackbox-targets.json
```
Задать адреса отслеживаемых сайтов в файле blackbox-targets.json
Отредактировав файл blackbox-targets.json
```
nano ./config/blackbox-targets.json
```

Скопировать файл ./config/alertmanager.yml.example ./config/alertmanager.yml
```
cp ./config/alertmanager.yml.example ./config/alertmanager.yml
```
Задать адрес отправки уведомлений и настройки почтового сервера в файле alertmanager.yml
```
nano ./config/alertmanager.yml
```
Запустить весь стек
```
docker-compose up -d
```
Открыть Grafana
```
http://localhost:3000
```
